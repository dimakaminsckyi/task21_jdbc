package com.epam.util;
import java.io.IOException;
import java.util.Properties;

public class PropertiesRead {

    private final Properties properties;

    public PropertiesRead(String propertiesPath) {
        this.properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(propertiesPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getValue(String key){
        return properties.getProperty(key);
    }
}

