package com.epam.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

    private static Logger log = LogManager.getLogger(Database.class);
    private static Database dbIsntance;
    private static Connection connection;
    private final String host;
    private final String username;
    private final String password;


    private Database() {
        PropertiesRead propertiesRead = new PropertiesRead("mysql.properties");
        host = propertiesRead.getValue("host");
        username = propertiesRead.getValue("username");
        password = propertiesRead.getValue("password");
    }

    public static Database getInstance(){
        if(dbIsntance==null){
            dbIsntance = new Database();
        }
        return dbIsntance;
    }

    public  Connection getConnection(){
        if(connection ==null){
            try {
                connection = DriverManager.getConnection( host, username, password );
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
        return connection;
    }

    public void shutDown(){
        if (connection != null){
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }
}
