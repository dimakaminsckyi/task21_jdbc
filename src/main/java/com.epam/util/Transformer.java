package com.epam.util;

import com.epam.model.DTO.CarClass;
import com.epam.model.DTO.Payment;
import com.epam.model.annotation.Column;
import com.epam.model.annotation.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {

    private static Logger log = LogManager.getLogger(Transformer.class);
    private final Class<T> clazz;

    public Transformer(Class<T> clazz){
        this.clazz = clazz;
    }

    public Object transofrmResultSetToDTO(ResultSet resultSet){
        Object entity = null;
        try{
            entity = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)){
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields){
                    if (field.isAnnotationPresent(Column.class)){
                        Column column = field.getAnnotation(Column.class);
                        String name = column.name();
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        if (fieldType == String.class){
                            field.set(entity , resultSet.getString(name));
                        }else if(fieldType == int.class){
                            field.set(entity , resultSet.getInt(name));
                        }else if(fieldType == Boolean.class){
                            field.set(entity , resultSet.getBoolean(name));
                        }else if(fieldType == CarClass.class){
                            field.set(entity , CarClass.valueOf(resultSet.getString(name).toUpperCase()));
                        }else if (fieldType == Payment.class){
                            field.set(entity , Payment.valueOf(resultSet.getString(name).toUpperCase()));
                        }
                    }
                }
            }
        } catch (InstantiationException | InvocationTargetException |
                NoSuchMethodException | SQLException | IllegalAccessException e) {
            log.error(e.getMessage());
        }

        return entity;
    }

}
