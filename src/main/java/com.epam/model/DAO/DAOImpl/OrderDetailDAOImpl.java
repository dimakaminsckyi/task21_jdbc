package com.epam.model.DAO.DAOImpl;

import com.epam.model.DAO.OrderDetailDAO;
import com.epam.model.DTO.OrderDetail;
import com.epam.util.Database;
import com.epam.util.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDetailDAOImpl implements OrderDetailDAO {

    private static Logger log = LogManager.getLogger(OrderDetailDAOImpl.class);
    private static final String SELECT_ALL = "SELECT * FROM order_detail";
    private static final String SELECT_BY_ID = "SELECT * FROM order_detail WHERE id = ?";
    private static final String CREATE = "INSERT INTO order_detail" +
            "(start_point, end_point, city,price, payment,date_of_order, rating, status_of_order)" +
            " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ";
    private static final String UPDATE = "UPDATE order_detail SET " +
            "start_point = ?, end_point = ?, city = ?, price = ?, payment = ?," +
            " date_of_order = ?, rating = ?, status_of_order = ? WHERE id = ? ";
    private static final String DELETE = "DELETE FROM order_detail WHERE id = ?";


    @Override
    public List<OrderDetail> findAll() throws SQLException {
        List<OrderDetail> orderDetails = new ArrayList<>();
        Connection connection = Database.getInstance().getConnection();
        try(Statement statement = connection.createStatement()) {
            try(ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {
                while (resultSet.next()){
                    orderDetails.add((OrderDetail) new Transformer(OrderDetail.class).transofrmResultSetToDTO(resultSet));
                }
            }
        }
        return orderDetails;
    }

    @Override
    public OrderDetail findById(Integer integer) throws SQLException {
        OrderDetail orderDetail = null;
        Connection connection = Database.getInstance().getConnection();
        try(PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, integer);
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()){
                    orderDetail = (OrderDetail) new Transformer(OrderDetail.class).transofrmResultSetToDTO(resultSet);
                }
            }
        }
        return orderDetail;
    }

    @Override
    public int create(OrderDetail entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setString(1 , entity.getStartPoint());
            statement.setString(2 , entity.getEndPoint());
            statement.setString( 3, entity.getCity());
            statement.setInt( 4, entity.getPrice());
            statement.setString( 5, entity.getPayment().toString().toLowerCase());
            statement.setString( 6, entity.getDateOfOrder());
            statement.setInt( 7, entity.getRating());
            statement.setBoolean( 8, entity.isStatus());
            result = statement.executeUpdate();
            return result;
        }
    }

    @Override
    public int update(OrderDetail entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1 , entity.getStartPoint());
            statement.setString(2 , entity.getEndPoint());
            statement.setString( 3, entity.getCity());
            statement.setInt( 4, entity.getPrice());
            statement.setString( 5, String.valueOf(entity.getPayment()).toLowerCase());
            statement.setString( 6, entity.getDateOfOrder());
            statement.setInt( 7, entity.getRating());
            statement.setBoolean( 8, entity.isStatus());
            statement.setInt(9 ,entity.getId());
            result = statement.executeUpdate();
        }
        return result;
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1 , integer);
            result = statement.executeUpdate();
        }
        return result;
    }
}
