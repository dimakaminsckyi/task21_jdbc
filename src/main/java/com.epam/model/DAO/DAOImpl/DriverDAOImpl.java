package com.epam.model.DAO.DAOImpl;

import com.epam.model.DAO.DriverDAO;
import com.epam.model.DTO.Driver;
import com.epam.util.Database;
import com.epam.util.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DriverDAOImpl implements DriverDAO {

    private static Logger log = LogManager.getLogger(DriverDAOImpl.class);
    private static final String SELECT_ALL = "SELECT * FROM driver";
    private static final String SELECT_BY_ID = "SELECT * FROM driver WHERE id = ?";
    private static final String CREATE = "INSERT INTO driver(driver_name, phone_number, driver_status, average_rating)" +
            " VALUES(?, ?, ?, ?) ";
    private static final String UPDATE = "UPDATE drier " +
            "SET driver_name = ?, phone_number = ?, driver_status = ? , average_rating = ? WHERE id = ? ";
    private static final String DELETE = "DELETE FROM driver WHERE id = ?";

    @Override
    public List<Driver> findAll() throws SQLException {
        List<Driver> drivers = new ArrayList<>();
        Connection connection = Database.getInstance().getConnection();
        try(Statement statement = connection.createStatement()) {
            try(ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {
                while(resultSet.next()){
                    drivers.add((Driver) new Transformer(Driver.class).transofrmResultSetToDTO(resultSet));
                }
            }

        }
        return drivers;
    }

    @Override
    public Driver findById(Integer integer) throws SQLException {
        Driver driver = null;
        Connection connection = Database.getInstance().getConnection();
        try(PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, integer);
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()){
                    driver = (Driver) new Transformer(Driver.class).transofrmResultSetToDTO(resultSet);
                }
            }
        }
        return driver;
    }

    @Override
    public int create(Driver entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getPhoneNumber());
            statement.setBoolean(3, entity.isDriverStatus());
            statement.setInt(4, entity.getAverageRating());
            result = statement.executeUpdate();
            return result;
        }
    }

    @Override
    public int update(Driver entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1 , entity.getName());
            statement.setString(2 , entity.getPhoneNumber());
            statement.setBoolean( 3, entity.isDriverStatus());
            statement.setInt( 4, entity.getAverageRating());
            statement.setInt(5 ,entity.getId());
            result = statement.executeUpdate();
        }
        return result;
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1 , integer);
            result = statement.executeUpdate();
        }
        return result;
    }
}
