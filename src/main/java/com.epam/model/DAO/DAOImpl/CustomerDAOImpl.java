package com.epam.model.DAO.DAOImpl;

import com.epam.model.DAO.CustomerDAO;
import com.epam.model.DTO.Customer;
import com.epam.util.Database;
import com.epam.util.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO {

    private static Logger log = LogManager.getLogger(CustomerDAOImpl.class);
    private static final String SELECT_ALL = "SELECT * FROM customer";
    private static final String SELECT_BY_ID = "SELECT * FROM customer WHERE id = ?";
    private static final String CREATE = "INSERT INTO customer(customer_name, phone_number) VALUES(?, ?) ";
    private static final String UPDATE = "UPDATE customer SET customer_name = ?, phone_number = ? WHERE id = ? ";
    private static final String DELETE = "DELETE FROM customer WHERE id = ?";

    @Override
    public List<Customer> findAll() throws SQLException {
        List<Customer> customers = new ArrayList<>();
        Connection connection = Database.getInstance().getConnection();
        try(Statement statement = connection.createStatement()) {
            try(ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {
                while(resultSet.next()){
                    customers.add((Customer) new Transformer(Customer.class).transofrmResultSetToDTO(resultSet));
                }
            }
        }
        return customers;
    }

    @Override
    public Customer findById(Integer integer) throws SQLException {
        Customer customer = null;
        Connection connection = Database.getInstance().getConnection();
        try(PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, integer);
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()){
                    customer = (Customer) new Transformer(Customer.class).transofrmResultSetToDTO(resultSet);
                }
            }
        }
        return customer;
    }

    @Override
    public int create(Customer entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getPhoneNumber());
            result = statement.executeUpdate();
            return result;
        }
    }

    @Override
    public int update(Customer entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1 , entity.getName());
            statement.setString(2 , entity.getPhoneNumber());
            statement.setInt(3 ,entity.getId());
            result = statement.executeUpdate();
        }
        return result;
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1 , integer);
            result = statement.executeUpdate();
        }
        return result;
    }
}
