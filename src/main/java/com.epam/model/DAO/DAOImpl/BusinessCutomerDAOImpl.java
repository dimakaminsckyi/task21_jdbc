package com.epam.model.DAO.DAOImpl;

import com.epam.model.DAO.BusinessCustomerDAO;
import com.epam.model.DTO.BusinessCustomer;
import com.epam.util.Database;
import com.epam.util.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BusinessCutomerDAOImpl implements BusinessCustomerDAO {

    private static final String SELECT_ALL = "SELECT * FROM business_customer";
    private static final String SELECT_BY_ID = "SELECT * FROM business_customer WHERE id = ?";
    private static final String CREATE = "INSERT INTO business_customer(business_customer_name, phone_number) VALUES(?, ?) ";
    private static final String UPDATE = "UPDATE business_customer " +
            "SET business_customer_name = ?, phone_number = ? WHERE id = ? ";
    private static final String DELETE = "DELETE FROM business_customer WHERE id = ?";

    @Override
    public List<BusinessCustomer> findAll() throws SQLException {
        List<BusinessCustomer> businessCustomers = new ArrayList<>();
        Connection connection = Database.getInstance().getConnection();
        try(Statement statement = connection.createStatement()) {
            try(ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {
                while(resultSet.next()){
                    businessCustomers.add(
                            (BusinessCustomer) new Transformer(BusinessCustomer.class)
                                    .transofrmResultSetToDTO(resultSet));
                }
            }
        }
        return businessCustomers;
    }

    @Override
    public BusinessCustomer findById(Integer integer) throws SQLException {
        BusinessCustomer businessCustomer = null;
        Connection connection = Database.getInstance().getConnection();
        try(PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, integer);
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()){
                    businessCustomer =
                            (BusinessCustomer) new Transformer(BusinessCustomer.class)
                                    .transofrmResultSetToDTO(resultSet);
                }
            }
        }
        return businessCustomer;
    }

    @Override
    public int create(BusinessCustomer entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getPhoneNumber());
            result = statement.executeUpdate();
            return result;
        }
    }

    @Override
    public int update(BusinessCustomer entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1 , entity.getName());
            statement.setString(2 , entity.getPhoneNumber());
            statement.setInt(3 ,entity.getId());
            result = statement.executeUpdate();
        }
        return result;
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1 , integer);
            result = statement.executeUpdate();
        }
        return result;
    }
}
