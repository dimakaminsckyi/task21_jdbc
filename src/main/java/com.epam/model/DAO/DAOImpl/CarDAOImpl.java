package com.epam.model.DAO.DAOImpl;

import com.epam.model.DAO.CarDAO;
import com.epam.model.DTO.Car;
import com.epam.util.Database;
import com.epam.util.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarDAOImpl implements CarDAO {

    private static final String SELECT_ALL = "SELECT * FROM car";
    private static final String SELECT_BY_ID = "SELECT * FROM car WHERE id = ?";
    private static final String CREATE = "INSERT INTO car(brand, model, class)" +
            " VALUES(?, ?, ?) ";
    private static final String UPDATE = "UPDATE car " +
            "SET brand = ?, model = ?, class = ? WHERE id = ? ";
    private static final String DELETE = "DELETE FROM car WHERE id = ?";


    @Override
    public List<Car> findAll() throws SQLException {
        List<Car> cars = new ArrayList<>();
        Connection connection = Database.getInstance().getConnection();
        try(Statement statement = connection.createStatement()) {
            try(ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {
                while(resultSet.next()){
                    cars.add((Car) new Transformer(Car.class).transofrmResultSetToDTO(resultSet));
                }
            }

        }
        return cars;
    }

    @Override
    public Car findById(Integer integer) throws SQLException {
        Car car = null;
        Connection connection = Database.getInstance().getConnection();
        try(PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, integer);
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()){
                    car = (Car) new Transformer(Car.class).transofrmResultSetToDTO(resultSet);
                }
            }
        }
        return car;
    }

    @Override
    public int create(Car entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setString(1, entity.getBrand());
            statement.setString(2, entity.getModel());
            statement.setString(3, entity.getCarClass().toString().toLowerCase());
            result = statement.executeUpdate();
            return result;
        }
    }

    @Override
    public int update(Car entity) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1, entity.getBrand());
            statement.setString(2, entity.getModel());
            statement.setString(3, entity.getCarClass().toString().toLowerCase());
            statement.setInt(4, entity.getId());
            result = statement.executeUpdate();
            return result;
        }
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        int result;
        try(PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1 , integer);
            result = statement.executeUpdate();
        }
        return result;
    }
}
