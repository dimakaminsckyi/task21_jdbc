package com.epam.model.DAO;

import com.epam.model.DTO.BusinessCustomer;

public interface BusinessCustomerDAO extends GeneralDAO<BusinessCustomer, Integer> {
}
