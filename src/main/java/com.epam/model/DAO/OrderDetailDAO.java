package com.epam.model.DAO;

import com.epam.model.DTO.OrderDetail;

public interface OrderDetailDAO extends GeneralDAO<OrderDetail, Integer> {
}
