package com.epam.model.DAO;

import com.epam.model.DTO.Customer;

public interface CustomerDAO extends GeneralDAO<Customer , Integer> {
}
