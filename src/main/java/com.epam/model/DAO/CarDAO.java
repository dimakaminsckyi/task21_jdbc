package com.epam.model.DAO;

import com.epam.model.DTO.Car;

public interface CarDAO extends GeneralDAO<Car, Integer> {
}
