package com.epam.model.DAO;

import com.epam.model.DTO.Driver;

public interface DriverDAO extends GeneralDAO<Driver, Integer> {
}
