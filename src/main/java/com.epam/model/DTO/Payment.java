package com.epam.model.DTO;

public enum Payment {
    CARD,
    CASH
}
