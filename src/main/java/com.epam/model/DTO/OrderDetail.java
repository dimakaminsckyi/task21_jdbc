package com.epam.model.DTO;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.Table;

@Table(name = "order_detail")
public class OrderDetail {

    @Column(name = "id")
    private int id;
    @Column(name = "start_point" , length = 45)
    private String startPoint;
    @Column(name = "end_point", length = 45)
    private String endPoint;
    @Column(name = "city", length = 20)
    private String city;
    @Column(name = "price", length = 250)
    private int price;
    @Column(name = "payment")
    private Payment payment;
    @Column(name = "date_of_order", length = 8)
    private String dateOfOrder;
    @Column(name = "rating",length = 5)
    private int rating;
    @Column(name = "status_of_order")
    private boolean status;

    public OrderDetail() {
    }

    public OrderDetail(String startPoint, String endPoint, String city
            , int price, Payment payment, String dateOfOrder, int rating) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.city = city;
        this.price = price;
        this.payment = payment;
        this.dateOfOrder = dateOfOrder;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public String getCity() {
        return city;
    }

    public int getPrice() {
        return price;
    }

    public Payment getPayment() {
        return payment;
    }

    public String getDateOfOrder() {
        return dateOfOrder;
    }

    public int getRating() {
        return rating;
    }

    public boolean isStatus() {
        return status;
    }

    public void setId(int id) {
        this.id = id;
    }
}
