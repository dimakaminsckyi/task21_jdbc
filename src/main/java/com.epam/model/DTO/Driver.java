package com.epam.model.DTO;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.Table;

@Table(name = "driver")
public class Driver {

    @Column(name = "id")
    private int id;
    @Column(name = "driver_name", length = 45)
    private String name;
    @Column(name = "phone_number", length = 10)
    private String phoneNumber;
    @Column(name = "driver_status")
    private boolean driverStatus;
    @Column(name = "average_rating", length = 5)
    private int averageRating;

    public Driver() {
    }

    public Driver(String name, String phoneNumber, int averageRating) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.averageRating = averageRating;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean isDriverStatus() {
        return driverStatus;
    }

    public int getAverageRating() {
        return averageRating;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", driverStatus=" + driverStatus +
                ", averageRating=" + averageRating +
                '}';
    }
}
