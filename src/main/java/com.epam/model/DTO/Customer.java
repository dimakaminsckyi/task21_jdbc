package com.epam.model.DTO;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.Table;

@Table(name = "customer")
public class Customer {

    @Column(name = "id")
    private int id;
    @Column(name = "customer_name", length = 45)
    private String name;
    @Column(name = "phone_number", length = 10)
    private String phoneNumber;

    public Customer() {
    }

    public Customer(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
