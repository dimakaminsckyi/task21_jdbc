package com.epam.model.DTO;

public enum CarClass {

    WAGON,
    COMFORT,
    MINIBUS,
    BUSINESS
}
