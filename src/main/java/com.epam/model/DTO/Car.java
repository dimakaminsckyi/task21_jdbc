package com.epam.model.DTO;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.Table;

@Table(name = "car")
public class Car {

    @Column(name = "id")
    private int id;
    @Column(name = "brand", length = 20)
    private String brand;
    @Column(name = "model", length = 20)
    private String model;
    @Column(name = "class")
    private CarClass carClass;

    public Car() {
    }

    public Car(String brand, String model, CarClass carClass) {
        this.brand = brand;
        this.model = model;
        this.carClass = carClass;
    }

    public Car(int id, String brand, String model, CarClass carClass) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.carClass = carClass;
    }

    public int getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public CarClass getCarClass() {
        return carClass;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", carClass=" + carClass +
                '}';
    }
}
