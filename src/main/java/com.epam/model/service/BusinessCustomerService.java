package com.epam.model.service;

import com.epam.model.DTO.BusinessCustomer;

public interface BusinessCustomerService extends GeneralService<BusinessCustomer, Integer> {
}
