package com.epam.model.service;

import java.sql.SQLException;

public interface GeneralService<T, ID> {

    void findAll() throws SQLException;

    void findById() throws SQLException;

    int create() throws SQLException;

    int update() throws SQLException;

    int delete() throws SQLException;
}
