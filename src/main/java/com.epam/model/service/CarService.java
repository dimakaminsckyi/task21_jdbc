package com.epam.model.service;

import com.epam.model.DTO.Car;

public interface CarService extends GeneralService<Car, Integer> {

}
