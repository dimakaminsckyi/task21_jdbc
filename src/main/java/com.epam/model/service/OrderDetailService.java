package com.epam.model.service;

import com.epam.model.DTO.OrderDetail;

public interface OrderDetailService extends GeneralService<OrderDetail, Integer> {
}
