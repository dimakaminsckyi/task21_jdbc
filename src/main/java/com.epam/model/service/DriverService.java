package com.epam.model.service;

import com.epam.model.DTO.Driver;

public interface DriverService extends GeneralService<Driver, Integer> {
}
