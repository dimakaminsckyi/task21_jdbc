package com.epam.model.service.impl;

import com.epam.model.DAO.DAOImpl.DriverDAOImpl;
import com.epam.model.DAO.DriverDAO;
import com.epam.model.DTO.Driver;
import com.epam.model.service.DriverService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Scanner;

public class DriverServiceImpl implements DriverService {

    private static Logger log = LogManager.getLogger(DriverServiceImpl.class);
    private DriverDAO dao;
    private Scanner scanner;

    public DriverServiceImpl() {
        dao = new DriverDAOImpl();
        scanner = new Scanner(System.in);
    }

    @Override
    public void findAll() throws SQLException {
        dao.findAll().forEach(log::info);
    }

    @Override
    public void findById() throws SQLException {
        log.info("Enter id : ");
        log.info(dao.findById(scanner.nextInt()));
    }

    @Override
    public int create() throws SQLException {
        return dao.create(generateDriver());
    }

    @Override
    public int update() throws SQLException {
        Driver driver = generateDriver();
        log.info("Enter id : ");
        int id = scanner.nextInt();
        driver.setId(id);
        return dao.update(driver);
    }

    @Override
    public int delete() throws SQLException {
        log.info("Enter id :");
        return dao.delete(scanner.nextInt());
    }


    private Driver generateDriver(){
        log.info("Enter driver new name : ");
        String name = scanner.next();
        log.info("Enter new phone number : ");
        String phoneNumber = scanner.next();
        log.info("Enter average rating : ");
        int rating = scanner.nextInt();
        return new Driver(name,phoneNumber,rating);
    }
}
