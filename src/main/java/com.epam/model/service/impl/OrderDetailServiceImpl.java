package com.epam.model.service.impl;

import com.epam.model.DAO.DAOImpl.OrderDetailDAOImpl;
import com.epam.model.DAO.OrderDetailDAO;
import com.epam.model.DTO.OrderDetail;
import com.epam.model.DTO.Payment;
import com.epam.model.service.OrderDetailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Scanner;

public class OrderDetailServiceImpl implements OrderDetailService {

    private static Logger log = LogManager.getLogger(CustomerServiceImpl.class);
    private OrderDetailDAO dao;
    private Scanner scanner;

    public OrderDetailServiceImpl() {
        dao = new OrderDetailDAOImpl();
        scanner = new Scanner(System.in);
    }

    @Override
    public void findAll() throws SQLException {
        dao.findAll().forEach(log::info);
    }

    @Override
    public void findById() throws SQLException {
        log.info("Enter id : ");
        log.info(dao.findById(scanner.nextInt()));
    }

    @Override
    public int create() throws SQLException {
        return dao.create(generateOrderDetail());
    }

    @Override
    public int update() throws SQLException {
        OrderDetail orderDetail = generateOrderDetail();
        log.info("Enter id : ");
        int id = scanner.nextInt();
        orderDetail.setId(id);
        return dao.update(orderDetail);
    }

    @Override
    public int delete() throws SQLException {
        log.info("Enter id :");
        return dao.delete(scanner.nextInt());
    }

    private OrderDetail generateOrderDetail(){
        log.info("Enter order start point : ");
        String startPoint = scanner.next();
        log.info("Enter end Point : ");
        String endPoint = scanner.next();
        log.info("Enter city : ");
        String city = scanner.next();
        log.info("Enter price :");
        int price = scanner.nextInt();
        log.info("Enter payment");
        Payment payment = Payment.valueOf(scanner.next().toUpperCase());
        log.info("Enter date :");
        String date = scanner.next();
        log.info("Enter rating");
        int rating = scanner.nextInt();
        return new OrderDetail(startPoint,endPoint,city,price,payment,date,rating);
    }
}
