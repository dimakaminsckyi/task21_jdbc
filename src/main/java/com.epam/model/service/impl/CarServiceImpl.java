package com.epam.model.service.impl;

import com.epam.model.DAO.CarDAO;
import com.epam.model.DAO.DAOImpl.CarDAOImpl;
import com.epam.model.DTO.Car;
import com.epam.model.DTO.CarClass;
import com.epam.model.service.CarService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Scanner;

public class CarServiceImpl implements CarService {

    private static Logger log = LogManager.getLogger(CarServiceImpl.class);
    private CarDAO dao;
    private Scanner scanner ;

    public CarServiceImpl() {
        dao = new CarDAOImpl();
        scanner = new Scanner(System.in);
    }

    @Override
    public void findAll() throws SQLException {
        dao.findAll().forEach(log::info);
    }

    @Override
    public void findById() throws SQLException {
        log.info("Enter id : ");
        log.info(dao.findById(scanner.nextInt()));
    }

    @Override
    public int create() throws SQLException {
        return dao.create(generateCar());
    }

    @Override
    public int update() throws SQLException {
        log.info("Enter car id :");
        int id = scanner.nextInt();
        Car car = generateCar();
        car.setId(id);
        return dao.update(car);
    }

    @Override
    public int delete() throws SQLException {
        log.info("Enter id : ");
        return dao.delete(scanner.nextInt());
    }

    private Car generateCar(){
        log.info("Enter car new brand : ");
        String brand = scanner.next();
        log.info("Enter new model : ");
        String model = scanner.next();
        log.info("Enter new Class :");
        CarClass carClass = CarClass.valueOf(scanner.next().toUpperCase());
        return new Car(brand,model,carClass);
    }
}
