package com.epam.model.service.impl;

import com.epam.model.DAO.CustomerDAO;
import com.epam.model.DAO.DAOImpl.CustomerDAOImpl;
import com.epam.model.DTO.Customer;
import com.epam.model.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Scanner;

public class CustomerServiceImpl implements CustomerService{

    private static Logger log = LogManager.getLogger(CustomerServiceImpl.class);
    private CustomerDAO dao;
    private Scanner scanner;

    public CustomerServiceImpl() {
        dao = new CustomerDAOImpl();
        scanner = new Scanner(System.in);
    }

    @Override
    public void findAll() throws SQLException {
        dao.findAll().forEach(log::info);
    }

    @Override
    public void findById() throws SQLException {
        log.info("Enter id : ");
        log.info(dao.findById(scanner.nextInt()));
    }

    @Override
    public int create() throws SQLException {
        return dao.create(generateCustomer());
    }

    @Override
    public int update() throws SQLException {
        Customer customer = generateCustomer();
        log.info("Enter id : ");
        int id = scanner.nextInt();
        customer.setId(id);
        return dao.update(customer);
    }

    @Override
    public int delete() throws SQLException {
        log.info("Enter id :");
        return dao.delete(scanner.nextInt());
    }

    private Customer generateCustomer(){
        log.info("Enter customer new name : ");
        String name = scanner.next();
        log.info("Enter phone number : ");
        String phoneNumber = scanner.next();
        return new Customer(name,phoneNumber);
    }
}
