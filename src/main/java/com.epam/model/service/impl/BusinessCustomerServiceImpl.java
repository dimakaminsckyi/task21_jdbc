package com.epam.model.service.impl;

import com.epam.model.DAO.BusinessCustomerDAO;
import com.epam.model.DAO.DAOImpl.BusinessCutomerDAOImpl;
import com.epam.model.DTO.BusinessCustomer;
import com.epam.model.service.BusinessCustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Scanner;

public class BusinessCustomerServiceImpl implements BusinessCustomerService {
    private static Logger log = LogManager.getLogger(CustomerServiceImpl.class);
    private BusinessCustomerDAO dao;
    private Scanner scanner;

    public BusinessCustomerServiceImpl() {
        dao = new BusinessCutomerDAOImpl();
        scanner = new Scanner(System.in);
    }

    @Override
    public void findAll() throws SQLException {
        dao.findAll().forEach(log::info);
    }

    @Override
    public void findById() throws SQLException {
        log.info("Enter id : ");
        log.info(dao.findById(scanner.nextInt()));
    }

    @Override
    public int create() throws SQLException {
        return dao.create(generateBusinessCustomer());
    }

    @Override
    public int update() throws SQLException {
        BusinessCustomer customer = generateBusinessCustomer();
        log.info("Enter id : ");
        int id = scanner.nextInt();
        customer.setId(id);
        return dao.update(customer);
    }

    @Override
    public int delete() throws SQLException {
        log.info("Enter id :");
        return dao.delete(scanner.nextInt());
    }

    private BusinessCustomer generateBusinessCustomer(){
        log.info("Enter customer new name : ");
        String name = scanner.next();
        log.info("Enter phone number : ");
        String phoneNumber = scanner.next();
        return new BusinessCustomer(name,phoneNumber);
    }
}
