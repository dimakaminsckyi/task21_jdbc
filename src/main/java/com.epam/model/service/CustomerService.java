package com.epam.model.service;

import com.epam.model.DTO.Customer;

public interface CustomerService extends GeneralService<Customer, Integer> {
}
