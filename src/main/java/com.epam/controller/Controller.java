package com.epam.controller;

import com.epam.model.service.*;
import com.epam.model.service.impl.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Scanner;

public class Controller {

    private static Logger log = LogManager.getLogger(Controller.class);
    private CarService carService;
    private DriverService driverService;
    private OrderDetailService orderDetailService;
    private CustomerService customerService;
    private BusinessCustomerService businessCustomerService;

    public Controller() {
        carService = new CarServiceImpl();
        driverService = new DriverServiceImpl() ;
        orderDetailService = new OrderDetailServiceImpl();
        customerService = new CustomerServiceImpl();
        businessCustomerService = new BusinessCustomerServiceImpl();
    }

    public void create(Scanner scanner) {
        showType();
        try {
            switch (scanner.next()){
                case "driver":
                    driverService.create();
                    break;
                case "car":
                    carService.create();
                    break;
                case "customer":
                    customerService.create();
                    break;
                case "order":
                    orderDetailService.create();
                    break;
                case "business":
                    businessCustomerService.create();
                    break;
                default:
                    log.error("Wrong Enter");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(Scanner scanner) {
        showType();
        try {
            switch (scanner.next()){
                case "driver":
                    driverService.update();
                    break;
                case "car":
                    carService.update();
                    break;
                case "customer":
                    customerService.update();
                    break;
                case "order":
                    orderDetailService.update();
                    break;
                case "business":
                    businessCustomerService.update();
                    break;
                default:
                    log.error("Wrong Enter");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Scanner scanner) {
        showType();
        try{
            switch (scanner.next()){
                case "driver":
                    driverService.delete();
                    break;
                case "car":
                    carService.delete();
                    break;
                case "customer":
                    customerService.delete();
                    break;
                case "order":
                    orderDetailService.delete();
                    break;
                case "business":
                    businessCustomerService.delete();
                    break;
                default:
                    log.error("Wrong Enter");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void findAll(Scanner scanner) {
        showType();
        try{
            switch (scanner.next()){
                case "driver":
                    driverService.findAll();
                    break;
                case "car":
                    carService.findAll();
                    break;
                case "customer":
                    customerService.findAll();
                    break;
                case "order":
                    orderDetailService.findAll();
                    break;
                case "business":
                    businessCustomerService.findAll();
                    break;
                default:
                    log.error("Wrong Enter");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void findById(Scanner scanner) {
        showType();
        try {
            switch (scanner.next()){
                case "driver":
                    driverService.findById();
                    break;
                case "car":
                    carService.findById();
                    break;
                case "customer":
                    customerService.findById();
                    break;
                case "order":
                    orderDetailService.findById();
                    break;
                case "business":
                    businessCustomerService.findById();
                    break;
                default:
                    log.error("Wrong Enter");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showType() {
        log.info("\ndriver\ncar\ncustomer\nbusiness\norder");
    }
}
